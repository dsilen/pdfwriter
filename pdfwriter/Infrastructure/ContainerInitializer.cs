﻿using AutoMapper;

namespace pdfwriter.Infrastructure;

public static class ContainerInitializer
{
    public static IServiceCollection AddWebComponents(this IServiceCollection services)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        return services;
    }

    public static IServiceCollection AddMediatrService(this IServiceCollection services)
    {
        services.AddMediatR(cfg =>
        {
            cfg.RegisterServicesFromAssembly(typeof(ContainerInitializer).Assembly);
            cfg.RegisterServicesFromAssembly(typeof(Business.Infrastructure.ContainerInitializer).Assembly);
        });
        return services;
    }

    public static IServiceCollection AddAutoMapper(this IServiceCollection services)
    {
        var mapper = AutoMapperFactory.CreateMapper();
        services.AddSingleton(mapper);
        return services;
    }

}