using MediatR;

namespace Common.EventModels;

public record CreatePdfEvent(
    string CustomerNumber,
    Guid DocumentNumber,
    string Text) : IRequest;
