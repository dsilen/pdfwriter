# pdfwriter

`PDFwriter` is an implementation of a code test for SEB.

It is written in C# for dotnet 7, using an aspnet core api frontend.
The frontend writes events to a mediator, which in turn event handlers listen on for specific events.

The project is structured as follows:
* pdfwriter is the startup project. Responsible for setting up IoC container and start the aspnet engine.
* Presentation is the project containing the web-related code; controllers, view models and the like.
* Business contains business logic. Event handlers and services.
* DataAccess contains "outgoing" external connections. Database-repositories, filesystem access etc.
* Common is used for common definitions and functionality. Business model classes (or DTOs), helpers and extension methods etc.
* Test - unittests for the rest of the solution.

## How to install and run

The solution is built on top of docker, as requested.

To build a docker image, run:

    docker build --tag pdfwriter .

The solution creates pdf-files in an output-folder which can be mapped to a docker volume if
you want to investigate what it produces.

Example of startup of a container with such a volume bound to the folder /tmp/files:

    docker run -it --rm --name pdfwriter -p 5000:5000 -v /tmp/files:/pdf pdfwriter

To connect to the service, open a web browser and browse to http://localhost:5000/.
You will be redirected to a swagger page which can be used to invoke the API.

## Considerations and stuff

This is of course a somewhat limited setup. Things missing are for instance error handling, authentication/ authorization
configuration services and proper storage in a database etc.

I used the `mediatr` package mostly for fun. I've heard of it from colleagues, and wanted to try it out.
In a real world scenario, if performance was important, I would probably divide the service in a receiving part (just the web api related things)
and a worker part, separated with a proper message queue, such as Kafka, and then deploy with kubernetes.

Instead of the mediator-pattern I often use reactive extensions for propagating events within a process.
Could be used here as well, of course, but I wanted to learn something new to make the exercise a bit more interesting. :)
