using System.ComponentModel.DataAnnotations;
#pragma warning disable CS8618

namespace Presentation.Models
{
    public class CreatePdfRequest
    {
        [Required]
        [RegularExpression(@"\d{6}-\d{4}")]
        public string CustomerNumber { get; set; }

        [Required]
        public Guid DocumentNumber { get; set; }
        
        [Required]
        [MaxLength(1024)]
        public string Text { get; set; }
        
    }
}
