﻿using Microsoft.Extensions.Logging;
using PdfSharpCore.Pdf;
// ReSharper disable TemplateIsNotCompileTimeConstantProblem

namespace DataAccess.Repositories;

public class PdfRepository : IPdfRepository
{
    private readonly ILogger<PdfRepository> _logger;
    private readonly string _pdfDir;
    
    public PdfRepository(ILogger<PdfRepository> logger)
    {
        _logger = logger;
        _pdfDir = Environment.GetEnvironmentVariable("PDFFOLDER") ?? ".";
    }
    
    public Task SavePdfAsync(PdfDocument document, string filename)
    {
        var outFilePath = Path.Combine(_pdfDir, filename);
        document.Save(outFilePath);
        _logger.LogInformation($"Saved pdf-document to path: \"{outFilePath}\".");
        return Task.CompletedTask;
    }
}

public interface IPdfRepository
{
    Task SavePdfAsync(PdfDocument document, string filename);
}