using Common.EventModels;
using DataAccess.Repositories;

namespace Business.Services;

/// <summary>
/// <inheritdoc />
/// </summary>
public class PdfService : IPdfService
{
    private readonly IPdfRepository _pdfRepository;

    public PdfService(IPdfRepository pdfRepository)
    {
        _pdfRepository = pdfRepository;
    }
    
    /// <summary>
    /// <inheritdoc />
    /// </summary>
    public async Task CreatePdfAsync(CreatePdfEvent @event)
    {
        var document = new PdfSharpCore.Pdf.PdfDocument();

        var pageNewRenderer = document.AddPage();

        var renderer = PdfSharpCore.Drawing.XGraphics.FromPdfPage(pageNewRenderer);

        // Render title
        renderer.DrawString(
            $"Regarding customer: {@event.CustomerNumber}"
            , new PdfSharpCore.Drawing.XFont("Arial", 25)
            , PdfSharpCore.Drawing.XBrushes.Black
            , new PdfSharpCore.Drawing.XPoint(40, 50)
        );

        // Render text message
        renderer.DrawString(
            @event.Text
            , new PdfSharpCore.Drawing.XFont("Arial", 12)
            , PdfSharpCore.Drawing.XBrushes.Black
            , new PdfSharpCore.Drawing.XPoint(12, 120)
        );
        
        await _pdfRepository.SavePdfAsync(document, $"{@event.DocumentNumber}.pdf");
    }
}

/// <summary>
/// PdfService can create and store different kinds of pdf-files.
/// </summary>
public interface IPdfService
{
    /// <summary>
    /// Create some kind of pdf message
    /// </summary>
    Task CreatePdfAsync(CreatePdfEvent @event);
}