﻿using System;
using System.Threading.Tasks;
using Business.Services;
using Common.EventModels;
using DataAccess.Repositories;
using Moq;
using NUnit.Framework;
using PdfSharpCore.Pdf;

namespace Test.Business.Services;

[TestFixture]
public class PdfServiceTest
{
    private Mock<IPdfRepository> _pdfRepositoryMock;
    private PdfService _testObj;

    [SetUp]
    public void SetUp()
    {
        _pdfRepositoryMock = new Mock<IPdfRepository>();
        _testObj = new PdfService(_pdfRepositoryMock.Object);
    }
    
    [Test]
    public async Task CreatePdfAsync_customerData_savesToRepository() {
        // Arrange
        var customerData = new CreatePdfEvent("customerNumber", Guid.NewGuid(), "Some text");        

        // Act
        await _testObj.CreatePdfAsync(customerData);

        // Assert
        _pdfRepositoryMock.Verify(x =>
                x.SavePdfAsync(It.IsAny<PdfDocument>(), $"{customerData.DocumentNumber}.pdf"),
            Times.Once);
    }
}