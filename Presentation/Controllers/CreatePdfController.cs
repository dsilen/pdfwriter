using AutoMapper;
using Business.EventHandlers;
using Common.EventModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;

namespace Presentation.Controllers;

[Route("[controller]")]
[ApiController]
public class CreatePdfController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public CreatePdfController(IMediator mediator, IMapper mapper)
    {
        _mediator = mediator;
        _mapper = mapper;
    }
        
    [HttpPost]
    public async Task<ActionResult> Post(CreatePdfRequest request)
    {
        var @event = _mapper.Map<CreatePdfEvent>(request);
        await _mediator.Send(@event);
        return Ok();
    }
}