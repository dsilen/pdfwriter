﻿FROM mcr.microsoft.com/dotnet/sdk:7.0
RUN apt-get update ; apt-get -y install fontconfig
WORKDIR /app
COPY . ./
RUN dotnet build
EXPOSE 5000/TCP
ENV PDFFOLDER /pdf
RUN mkdir /pdf ; chmod a+rwx /pdf
CMD dotnet run --project pdfwriter --urls 'http://*:5000'
