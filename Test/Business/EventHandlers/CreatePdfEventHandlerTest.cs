﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Business.EventHandlers;
using Business.Services;
using Common.EventModels;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace Test.Business.EventHandlers;

[TestFixture]
public class CreatePdfEventHandlerTest
{
    private Mock<IPdfService> _pdfServiceMock;
    private Mock<ILogger<CreatePdfEventHandler>> _logMock;
    private CreatePdfEventHandler _testObj;

    [SetUp]
    public void SetUp()
    {
        _pdfServiceMock = new Mock<IPdfService>();
        _logMock = new Mock<ILogger<CreatePdfEventHandler>>();
        _testObj = new CreatePdfEventHandler(_pdfServiceMock.Object, _logMock.Object);
    }

    [Test]
    public async Task Handle_customerData_invokesCreatePdfAsync()
    {
        // Arrange
        var customerData = new CreatePdfEvent("customerNumber", Guid.NewGuid(), "Some text");        
        
        // Act
        await _testObj.Handle(customerData, new CancellationToken());

        // Assert
        _pdfServiceMock.Verify(x => x.CreatePdfAsync(customerData), Times.Once);
    }
}