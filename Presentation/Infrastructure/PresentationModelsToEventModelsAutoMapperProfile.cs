using AutoMapper;

namespace Presentation.Infrastructure;

public class PresentationModelsToEventModelsAutoMapperProfile : Profile
{
    public PresentationModelsToEventModelsAutoMapperProfile()
    {
        CreateSportRadarHostMap();
    }

    private void CreateSportRadarHostMap()
    {
        CreateMap<Models.CreatePdfRequest, Common.EventModels.CreatePdfEvent>()
            .ReverseMap();
    }
}