﻿using Business.Infrastructure;
using DataAccess.Infrastructure;
using pdfwriter.Infrastructure;

namespace pdfwriter;

public static class Program
{
    public static async Task Main(string[] args)
    {
        var app = RegisterApplication(args);
        await app.RunAsync();
    }
    
    static WebApplication RegisterApplication(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Services
            .AddCors(options =>
            {
                options.AddPolicy("allowAllHosts", policy => policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            })
            .AddBusinessComponents()
            .AddDataAccessComponents()
            .AddWebComponents()
            .AddAutoMapper()
            .AddMediatrService();

        var app = builder.Build();
        app.MapControllers();
        app.UseSwagger();
        app.UseSwaggerUI();
        app.Map("/", () => Results.Redirect("/swagger")).ExcludeFromDescription();

        return app;
    }
}


