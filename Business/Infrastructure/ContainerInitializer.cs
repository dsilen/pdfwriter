﻿using Business.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Business.Infrastructure;

public static class ContainerInitializer
{
    public static IServiceCollection AddBusinessComponents(this IServiceCollection services)
        {
            services.AddScoped<IPdfService, PdfService>();
            return services;
        }
}