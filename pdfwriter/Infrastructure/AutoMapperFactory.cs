﻿using AutoMapper;
using Presentation.Infrastructure;

namespace pdfwriter.Infrastructure;

public static class AutoMapperFactory
{
    public static IMapper CreateMapper()
    {
        var config = new MapperConfiguration(c =>
        {
            c.AddProfile<PresentationModelsToEventModelsAutoMapperProfile>();
        });

        config.AssertConfigurationIsValid();
        return config.CreateMapper();
    }
}