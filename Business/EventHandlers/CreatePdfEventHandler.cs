﻿using Business.Services;
using Common.EventModels;
using MediatR;
using Microsoft.Extensions.Logging;
// ReSharper disable TemplateIsNotCompileTimeConstantProblem

namespace Business.EventHandlers;

public class CreatePdfEventHandler : IRequestHandler<CreatePdfEvent>
{
    private readonly IPdfService _pdfService;
    private readonly ILogger<CreatePdfEventHandler> _logger;

    public CreatePdfEventHandler(IPdfService pdfService,
        ILogger<CreatePdfEventHandler> logger)
    {
        _pdfService = pdfService;
        _logger = logger;
    }
    
    public async Task Handle(CreatePdfEvent @event, CancellationToken cancellationToken)
    {
        _logger.LogInformation($"Handling CreatePdfEvent. DocumentNumber: {@event.DocumentNumber}");
        await _pdfService.CreatePdfAsync(@event);
        _logger.LogInformation($"Handling CreatePdfEvent done. DocumentNumber: {@event.DocumentNumber}");
    }
}