﻿using DataAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace DataAccess.Infrastructure;

public static class ContainerInitializer
{
    public static IServiceCollection AddDataAccessComponents(this IServiceCollection services)
        {
            services.AddScoped<IPdfRepository, PdfRepository>();
            return services;
        }
}